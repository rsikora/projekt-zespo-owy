﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Isi2.Projekt.Data
{
    public class CategoryRepository
    {
        private Entities entities = new Entities();

        public SaveContract AddCategories(int entryId, List<CategoryContract> categories)
        {
            SaveContract result = new SaveContract();
            foreach (var category in categories)
            {
                entities.cmex_categories_entry.Add(new cmex_categories_entry()
                {
                    cmex_id = entities.cmex_categories_entry.Count() + categories.IndexOf(category),
                    entx_id = entryId,
                    catx_id = category.Id
                });
            }

            entities.SaveChanges();
            result.Success = true;

            return result;
        }

        public SaveContract Create(CategoryContract category)
        {
            SaveContract result = new SaveContract();
            catx_categories tempCat = new catx_categories()
            {
                catx_id = GetCategories(null,null).Items.Last().Id+1,
                name = category.Name
            };

            CategoryFilterContract filter = new CategoryFilterContract();
            filter.Name = tempCat.name;
            var foundCategory = GetCategories(filter, null);
            if (foundCategory.Count == 1)
            {
                result.Errors = "Istnieje kategoria o tej nazwie";
                return result;
            }

            entities.catx_categories.Add(tempCat);
            entities.SaveChanges();
            result.Success = true;

            return result;
        }

        //public string Delete(int id)
        //{
        //    CategoryFilterContract filter = new CategoryFilterContract();
        //    filter.Id = id;
        //    var foundCategory = GetCategories(filter,null);
        //    if (foundCategory.Count == 1)
        //    {
        //        catx_categories tempCat = new catx_categories()
        //        {
        //            catx_id = id,
        //            name = foundCategory.Items[0].Name
        //        };
        //        entities.catx_categories.Remove(tempCat);
        //        entities.SaveChanges();
        //    }
                    
        //    return "";
        //}

        public PagedListResultContract<CategoryContract> GetCategories(CategoryFilterContract filter, PagedListRequestContract paging)
        {
            var query = entities.catx_categories.AsQueryable();

            if (filter != null)
            {
                if (filter.Id.HasValue)
                    query = query.Where(c => c.catx_id == filter.Id);
                if (!String.IsNullOrEmpty(filter.Name))
                    query = query.Where(c => c.name == filter.Name);
            }

            var count = query.Count();

            if (paging != null)
            {
                query = query.OrderBy(c => c.name).Skip(paging.Page * paging.PageSize).Take(paging.Take);
            }

            var result = query.Select(c => new CategoryContract
            {
                Id = c.catx_id,
                Name = c.name
            }).ToList();

            return new PagedListResultContract<CategoryContract> { Count = count, Items = result };
        }

        public PagedListResultContract<EntryCategoriesContract> GetEntryCategories(EntryCategoriesFilterContract filter, PagedListRequestContract paging)
        {
            var query = entities.cmex_categories_entry.AsQueryable();

            if (filter != null)
            {
                if (filter.Id.HasValue)
                    query = query.Where(c => c.cmex_id == filter.Id);
                if (filter.EntryId.HasValue)
                    query = query.Where(c => c.entx_id == filter.EntryId);
                if (filter.CategoryId.HasValue)
                    query = query.Where(c => c.catx_id == filter.CategoryId);
            }

            var count = query.Count();

            if (paging != null)
            {
                query = query.OrderBy(c => c.cmex_id).Skip(paging.Page * paging.PageSize).Take(paging.Take);
            }

            var result = query.Select(c => new EntryCategoriesContract
            {
                Id = c.cmex_id,
                EntryId = c.entx_id,
                CategoryId = c.catx_id
            }).ToList();

            return new PagedListResultContract<EntryCategoriesContract> { Count = count, Items = result };
        }
    }
}
